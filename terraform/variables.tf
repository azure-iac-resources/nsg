# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "default-rg"
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = "default"
}

variable "virtual_network_name" {
  description = "virtual network name"
  type = string
  default = "default"
}

# Azure NIC Name 
variable "nsg_name" {
  description = "NSG Name"
  type = string
  default = "default-nsg-name"
}

variable "nsg-security-rule-name" {
  description = "nsg security rule name"
  type = string
  default = "default"
}

variable "network_interface_name" {
  description = "network interface name"
  type = string
  default = "network_interface_name"
}