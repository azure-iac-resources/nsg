data "azurerm_resource_group" "rg"{
  name = var.resource_group_name
}

data "azurerm_network_interface" "nic"{
  name                = var.network_interface_name
  resource_group_name = data.azurerm_resource_group.rg.name

  depends_on = [data.azurerm_resource_group.rg]
}