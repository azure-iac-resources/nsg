# Create nsg
module "nsg" {
  source                  = "git::https://gitlab.com/azure-iac2/root-modules.git//nsg"
  nsg_name                = var.nsg_name  
  business_divsion        = var.business_divsion
  environment             = var.environment
  resource_group_name     = data.azurerm_resource_group.rg.name
  location                = data.azurerm_resource_group.rg.location
  nsg-security-rule-name  = var.nsg-security-rule-name
}

data "azurerm_network_security_group" "nsg" {
  name                = "${var.business_divsion}-${var.environment}-${var.nsg_name}"
  resource_group_name = data.azurerm_resource_group.rg.name

  depends_on = [module.nsg]
}

module "nsg-association" {
  source                    = "git::https://gitlab.com/azure-iac2/root-modules.git//nsg-association"
  network_interface_id      = data.azurerm_network_interface.nic.id
  network_security_group_id = data.azurerm_network_security_group.nsg.id

  depends_on = [data.azurerm_network_security_group.nsg]
}